import org.junit.Assert;
import org.junit.Test;

public class NumberRowSupplierTest {
    @Test
    public void first() {
        int[] t={4,5,6};
        Assert.assertEquals(t, Calculator.NumberRowSupplier.predefinedInt2());
    }

    @Test
    public void second() {
        int[] t={7,8,9};
        Calculator.NumberRowSupplier supplier=new Calculator.NumberRowSupplier();
        Assert.assertEquals(t,supplier.predefinedInt3());
    }
}
