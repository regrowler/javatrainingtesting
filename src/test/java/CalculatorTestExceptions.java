import org.junit.Assert;
import org.junit.Test;

public class CalculatorTestExceptions {

    @Test(expected =IllegalArgumentException.class )
    public void root() {

        Assert.assertNull(Calculator.root(-1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void devide() {
        Assert.assertNull(Calculator.devide(1,0));
    }

    @Test(expected = ArithmeticException.class)
    public void devide2() {
        Assert.assertNull(Calculator.devide(Double.MAX_VALUE,0.1));
    }
    @Test(expected = ArithmeticException.class)
    public void sum() {
        Assert.assertNull(Calculator.sum(Long.MAX_VALUE,Long.MAX_VALUE));
    }
    @Test(expected = ArithmeticException.class)
    public void sum2() {
        Assert.assertNull(Calculator.sum(Integer.MAX_VALUE,Integer.MAX_VALUE));
    }
    @Test(expected = ArithmeticException.class)
    public void sum3() {
        Assert.assertNull(Calculator.sum(Double.MAX_VALUE,Double.MAX_VALUE));
    }

    @Test(expected = ArithmeticException.class)
    public void subtract() {
        Assert.assertNull(Calculator.subtract(Long.MAX_VALUE,-Long.MAX_VALUE));
    }
    @Test(expected = ArithmeticException.class)
    public void subtract2() {
        Assert.assertNull(Calculator.subtract(Integer.MAX_VALUE,-Integer.MAX_VALUE));
    }
    @Test(expected = ArithmeticException.class)
    public void subtract3() {
        Assert.assertNull(Calculator.subtract(Double.MAX_VALUE,-Double.MAX_VALUE));
    }
}