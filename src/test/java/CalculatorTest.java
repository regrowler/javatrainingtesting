import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;


import java.time.Duration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

////
@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {

    @Test
    public void sumArray() {
        int[] m={1,1,1};
        Calculator.NumberRowSupplier supplier= Mockito.mock(Calculator.NumberRowSupplier.class);
        Mockito.when(supplier.predefinedInt4()).thenReturn(m);
        assertEquals(3,Calculator.sumArray1(supplier));

    }


    @Test
    public void root() {
        double r = Calculator.root(4);
        assertEquals(r, 2.0d,0.0d);
    }

    @Test(timeout = 2000)
    public void isPrime() {
        boolean res = Calculator.isPrime(1);
        assertTrue(res);

    }


    @Test
    public void fibon() {
        assertEquals(2, Calculator.fibon(3));
        assertEquals(8, Calculator.fibon(6));
        assertEquals(610, Calculator.fibon(15));
    }

    @Test
    public void sum() {
        long a = 2;
        long b = 2;
        float a1 = 2;
        float b1 = 2;
        double a2 = 2.0;
        double b2 = 2;
        assertEquals(Calculator.sum(2, 2), 4);
        assertEquals(Calculator.sum(a, b), 4);
        assertEquals(Calculator.sum(a1, b1), 4,0.0d);
        assertEquals(Calculator.sum(a2, b2), 4.0,0.0d);
    }

    @Test
    public void multiply() {
        long a = 2;
        long b = 2;
        float a1 = 2;
        float b1 = 2;
        double a2 = 2.0;
        double b2 = 2;
        assertEquals(Calculator.multiply(2, 2), 4);
        assertEquals(Calculator.multiply(a, b), 4);
        assertEquals(Calculator.multiply(a1, b1), 4,0.0d);
        assertEquals(Calculator.multiply(a2, b2), 4.0,0.0d);
    }

    @Test
    public void devide() {
        long a = 2;
        long b = 2;
        float a1 = 2;
        float b1 = 2;
        double a2 = 2.0;
        double b2 = 2;
        assertEquals(Calculator.devide(2, 1), 2,0.0d);
        assertEquals(Calculator.devide(a, b), 1,0.0d);
        assertEquals(Calculator.devide(a1, b1), 1,0.0d);
        assertEquals(Calculator.devide(a2, b2), 1.0,0.0d);
    }

    @Test
    public void subtract() {
        long a = 2;
        long b = 2;
        float a1 = 2;
        float b1 = 1;
        double a2 = 2.0;
        double b2 = 2;
        assertEquals(Calculator.subtract(2, 2), 0);
        assertEquals(Calculator.subtract(a, b), 0);
        assertEquals(Calculator.subtract(a1, b1), 1,0.0d);
        assertEquals(Calculator.subtract(a2, b2), 0.0,0.0d);
    }
}