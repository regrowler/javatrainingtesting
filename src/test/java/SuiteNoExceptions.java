import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorTest.class,CalculatorPowerMockite.class,NumberRowSupplierTest.class})
public class SuiteNoExceptions {
}
