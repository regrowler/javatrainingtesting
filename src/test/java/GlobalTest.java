import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class GlobalTest {
    public static void main(String[] m){
        testNoExceptions();
        testExceptions();
    }
    public static void testExceptions(){
        Result result = JUnitCore.runClasses(SuiteExceptions.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
            failure.getException().printStackTrace();
        }
    }
    public static void testNoExceptions(){
        Result result = JUnitCore.runClasses(SuiteNoExceptions.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
            failure.getException().printStackTrace();
        }
    }
}
