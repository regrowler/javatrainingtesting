import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;


@PrepareForTest({Calculator.class,Calculator.NumberRowSupplier.class})
@RunWith(PowerMockRunner.class)
public class CalculatorPowerMockite {
    @Test
    public void sumArray1() {
        int[] m = {1, 1, 1};
        Calculator.NumberRowSupplier supplier = PowerMockito.spy(new Calculator.NumberRowSupplier());
        try {
            PowerMockito.doReturn(m).when(supplier, "predefinedInt5");
            assertEquals(3, Calculator.sumArray1(supplier));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sumArray2() {
        int[] m = {1, 1, 1};
        PowerMockito.mockStatic(Calculator.NumberRowSupplier.class);
        try {
            PowerMockito.when(Calculator.NumberRowSupplier.predefinedInt2()).thenReturn(m);
            assertEquals(3, Calculator.sumArray(Calculator.NumberRowSupplier.predefinedInt2()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sumArray3() {
        int[] m = {1, 1, 1};
        final Calculator.NumberRowSupplier supplier=PowerMockito.mock(Calculator.NumberRowSupplier.class);
        PowerMockito.when(supplier.predefinedInt3()).thenReturn(m);
        assertEquals(3, Calculator.sumArray(supplier.predefinedInt3()));

    }
}
