import jdk.nashorn.internal.ir.annotations.Ignore;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

public class Calculator {
    private static final Logger log = Logger.getLogger(Calculator.class);
    public static int sumArray1(NumberRowSupplier supplier) {
          return   sumArray(supplier.predefinedInt4());
    }
    public static int sumArray(int[] mass){
        log.info("sumArray "+mass);
        int res= Arrays.stream(mass).reduce((ints, ints2) -> ints+ints2).getAsInt();
        log.info("sumArray res: "+res);
        return res;
    }
    public static double root(double arg) throws IllegalArgumentException {
        if (arg < 0) {
            log.error("root:" + IllegalArgumentException.class);
            throw new IllegalArgumentException();
        }
        log.info("root:" + arg);
        double res = Math.sqrt(arg);
        log.info("root res:" + res);
        return res;
    }

    public static boolean isPrime(long arg) {
        log.info("isPrime:" + arg);
        boolean res = false;
        for (int i = 2; i < arg; i++) {
            if (arg % i == 0) {
                log.info("isPrime res:" + false);
                return false;
            }
        }
        log.info("isPrime res:" + true);
        return true;
    }

    public static long fibon(long index) {
        log.info("fibon:" + index);
        int a = 1;
        int b = 1;
        int sumFib = a;
        for (int i = 2; i < index; i++) {
            sumFib = a + b;
            a = b;
            b = sumFib;
        }
        log.info("fibon res:" + index);
        return sumFib;
    }

    public static int sum(int f, int s) throws ArithmeticException {
        log.info("sum:" + f + " " + s);
        long ff = (long) f;
        long ss = (long) s;
        long res = ff + ss;
        if (isInBounds(res)) {
            log.error("sum err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            int res1=f+s;
            log.info("sum res:"+res1);
            return res1;
        }
    }

    public static long sum(long f, long s) throws ArithmeticException {
        log.info("sum:" + f + " " + s);
        BigInteger ff = BigInteger.valueOf(f);
        BigInteger ss = BigInteger.valueOf(s);
        BigInteger res = ff.add(ss);
        if (isInBounds(res)) {
            log.error("sum err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            long res1=f+s;
            log.info("sum res:"+res1);
            return res1;
        }
    }

    public static double sum(double f, double s) throws ArithmeticException {
        log.info("sum:" + f + " " + s);
        BigDecimal ff = BigDecimal.valueOf(f);
        BigDecimal ss = BigDecimal.valueOf(s);
        BigDecimal res = ff.add(ss);
        if (isInBounds(res)) {
            log.error("sum err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            double res1=f+s;
            log.info("sum res:"+res1);
            return res1;
        }
    }

    public static int multiply(int f, int s) throws ArithmeticException {
        log.info("multiply:" + f + " " + s);
        long ff = (long) f;
        long ss = (long) s;
        long res = ff * ss;
        if (isInBounds(res)) {
            log.error("multiply err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            int res1=f*s;
            log.info("multiply res:"+res1);
            return res1;
        }
    }

    public static long multiply(long f, long s) throws ArithmeticException {
        log.info("multiply:" + f + " " + s);
        BigInteger ff = BigInteger.valueOf(f);
        BigInteger ss = BigInteger.valueOf(s);
        BigInteger res = ff.multiply(ss);
        if (isInBounds(res)) {
            log.error("multiply err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            long res1=f*s;
            log.info("multiply res:"+res1);
            return res1;
        }
    }

    public static double multiply(double f, double s) throws ArithmeticException {
        log.info("multiply:" + f + " " + s);
        BigDecimal ff = BigDecimal.valueOf(f);
        BigDecimal ss = BigDecimal.valueOf(s);
        BigDecimal res = ff.multiply(ss);
        if (isInBounds(res)) {
            log.error("multiply err:" + ArithmeticException.class + "overflow");
            throw new ArithmeticException("overflow");
        } else {
            double res1=f*s;
            log.info("multiply res:"+res1);
            return res1;
        }
    }

    public static double devide(int f, int s) throws ArithmeticException, IllegalArgumentException {
        log.info("devide:" + f + " " + s);
        if (s == 0) throw new IllegalArgumentException();
        double ff = (double) f;
        double ss = (double) s;
        double res = ff / ss;
        if (isInBounds(res)) {
            log.error("devide err:" + ArithmeticException.class+"overflow");
            throw new ArithmeticException("overflow");
        } else {
            double res1 = 0d;
            res1 += f;
            f /= s;
            log.info("devide res:"+res1);
            return res1;
        }
    }

    public static double devide(double f, double s) throws ArithmeticException, IllegalArgumentException {
        log.info("devide:" + f + " " + s);
        if (s == 0) throw new IllegalArgumentException();
        BigDecimal ff = BigDecimal.valueOf(f);
        BigDecimal ss = BigDecimal.valueOf(s);
        BigDecimal res = ff.add(ss);
        if (isInBounds(res)) {
            log.error("devide err:" + ArithmeticException.class+"overflow");
            throw new ArithmeticException("overflow");
        } else {
            double res1 = 0d;
            res1 += f;
            res1 /= s;
            log.info("devide res:"+res1);
            return res1;
        }
    }

    public static int subtract(int f, int s) throws ArithmeticException {
        log.info("subtract:" + f + " " + s);
        long ff = (long) f;
        long ss = (long) s;
        long res = ff - ss;
        if (isInBounds(res)) {
            log.error("subtract err:" + ArithmeticException.class+"overflow");
            throw new ArithmeticException("overflow");
        } else {
            int res1=f-s;
            log.info("subtract res:"+res1);
            return res1;
        }
    }

    public static long subtract(long f, long s) throws ArithmeticException {
        log.info("subtract:" + f + " " + s);
        BigInteger ff = BigInteger.valueOf(f);
        BigInteger ss = BigInteger.valueOf(s);
        BigInteger res = ff.subtract(ss);
        if (isInBounds(res)) {
            log.error("subtract err:" + ArithmeticException.class+"overflow");
            throw new ArithmeticException("overflow");
        } else {
            long res1=f-s;
            log.info("subtract res:"+res1);
            return res1;
        }
    }

    public static double subtract(double f, double s) throws ArithmeticException {
        log.info("subtract:" + f + " " + s);
        BigDecimal ff = BigDecimal.valueOf(f);
        BigDecimal ss = BigDecimal.valueOf(s);
        BigDecimal res = ff.subtract(ss);
        if (isInBounds(res)) {
            log.error("subtract err:" + ArithmeticException.class+"overflow");
            throw new ArithmeticException("overflow");
        } else {
            double res1=f-s;
            log.info("subtract res:"+res1);
            return res1;
        }
    }

    private static boolean isInBounds(BigInteger res) {
        return res.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0 || res.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0;
    }

    private static boolean isInBounds(long res) {
        return res > Integer.MAX_VALUE || res < Integer.MIN_VALUE;
    }

    private static boolean isInBounds(double res) {
        return res > Float.MAX_VALUE || res < Float.MIN_VALUE;
    }

    private static boolean isInBounds(BigDecimal res1) {
        BigDecimal res = res1.abs();
        if (res1.compareTo(BigDecimal.valueOf(0.0)) == 0) return false;
        return res.compareTo(BigDecimal.valueOf(Double.MAX_VALUE)) > 0 || res.compareTo(BigDecimal.valueOf(Double.MIN_VALUE)) < 0;
    }

    public static class NumberRowSupplier {
        public static  int[] predefinedInt2(){
            int[] t={4,5,6};
            return t;
        }
        public final int[] predefinedInt3(){
            int[] t={7,8,9};
            return t;
        }
        public int[] predefinedInt4(){
            return predefinedInt5();
        }

        private int[] predefinedInt5(){
            int[] t={10,11,12};
            return t;
        }
    }
}
